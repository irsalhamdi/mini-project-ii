# IT-BOOTCAMP-BRI-PROJECT-II

## CONFIGURATION

Berikut beberapa konfigurasi yang perlu dilakukan untuk menjalankan project ini
- Silahkan untuk melakukan clone pada repository ini
- Lakukan pembuatan suatu basis data baru dengan nama satkom_indo
- Lakukan import file dari table satkom_indo.sql
- Silahkan buat file .env pada project yang telah di clone
- Salin konfigurasi dari file .env-example dan sesuaikan dengan konfigurasi anda
- Silahkan download documentation api pada link berikut
- Jalankan script di bawah ini pada terminal untuk mengunduh package go

```
go mod download
go mod tidy
```
- Jalankan script di bawah ini pada terminal untuk menjalankan file main.go
```
go run main.go
```

## AUTHOR

Irsal Hamdi

## ACKONOWLEDGMENTS

- Bank Rakyat Indonesia(BRI)
- SATKOM INDO
- dibimbing.id
- Mas salman dan Mas Bowo
- Teman-teman IT BOOTCAMP BRI
